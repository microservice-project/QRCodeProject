import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentComponent } from './payment/payment.component';
import { QrcodeComponent } from './qrcode/qrcode.component';
import { ScanComponent } from './scan/scan.component';

const routes: Routes = [
  { path: '', component: QrcodeComponent },
  { path: 'qrcode', component: QrcodeComponent },
  { path: 'payment', component: PaymentComponent },
  { path: 'scan', component: ScanComponent }
  // { path: '**', component: QrcodeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
