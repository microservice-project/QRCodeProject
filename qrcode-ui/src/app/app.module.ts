import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { QrcodeComponent } from './qrcode/qrcode.component';
import { PaymentComponent } from './payment/payment.component';
import { ScanComponent } from './scan/scan.component';
import { HttpClientModule } from '@angular/common/http';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { AppUiModule } from './app-ui.module';
// import { MatDialogModule } from '@angular/material/dialog';



@NgModule({
  declarations: [AppComponent, QrcodeComponent, PaymentComponent, ScanComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatTabsModule,
    HttpClientModule,
    ZXingScannerModule,
    AppUiModule
    // MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
