import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { QRCode } from './QRCode';

@Injectable({
  providedIn: 'root'
})
export class QrcodeService {

constructor(private qrcodeService: HttpClient) { }

public generateQRCode(): Observable<QRCode> {
  return this.qrcodeService.get<QRCode>('http://172.20.10.4:8080/generate/qrcode');
}

}
