import { Component, OnInit } from '@angular/core';
import { QRCode } from './QRCode';
import { QrcodeService } from './qrcode.service';

@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.component.html',
  styleUrls: ['./qrcode.component.scss']
})
export class QrcodeComponent implements OnInit {

  constructor(private qrcodeService: QrcodeService) { }

  public qrcode: string = "Hello";
  ngOnInit() {
    this.qrcodeService.generateQRCode().subscribe((response: QRCode) => {
      this.qrcode = response.qrcode;
      console.log(this.qrcode);

    })
  }

}
