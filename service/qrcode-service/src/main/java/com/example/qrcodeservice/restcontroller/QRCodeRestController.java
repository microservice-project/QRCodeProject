package com.example.qrcodeservice.restcontroller;

import com.example.qrcodeservice.restcontroller.request.PaymentRequest;
import com.example.qrcodeservice.restcontroller.response.PaymentResponse;
import com.example.qrcodeservice.restcontroller.response.QRCodeResponse;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.UUID;

@RestController
@CrossOrigin(value = "*")
public class QRCodeRestController {


    @PostMapping("payment")
    public ResponseEntity<PaymentResponse> payment(
        @RequestBody PaymentRequest payment
        ) {


        return ResponseEntity.ok(new PaymentResponse(payment.getReceiverId(), payment.getAmount(),payment.getDescription()));
    }

    @GetMapping("generate/qrcode")
    public ResponseEntity<QRCodeResponse> generateQRCode(
        @RequestParam(value = "width", defaultValue = "500") String widthParam,
        @RequestParam(value = "height", defaultValue = "500") String heightParam
    ) {

        String receiverId = UUID.randomUUID().toString().replaceAll("-", "");

        int width = Integer.valueOf(widthParam);
        int height = Integer.valueOf(heightParam);

        byte[] qrcode = generateQRCode(receiverId, width, height);

        return new ResponseEntity<>(new QRCodeResponse(Base64.getEncoder().encodeToString(qrcode)), HttpStatus.OK);
    }

    private Logger logger = LoggerFactory.getLogger(QRCodeRestController.class);

    public byte[] generateQRCode(String receiverId, int width,  int height) {
        try {
            String url = "http://172.20.10.4:4200/payment?receiverId="+receiverId;
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix bitMatrix = qrCodeWriter.encode(url, BarcodeFormat.QR_CODE, width, height);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(bitMatrix, "PNG", byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        } catch (WriterException e) {
            logger.error(e.getMessage(), e);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }
}
