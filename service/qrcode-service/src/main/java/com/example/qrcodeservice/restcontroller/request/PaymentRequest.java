package com.example.qrcodeservice.restcontroller.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import jdk.nashorn.internal.objects.annotations.Getter;
import lombok.Data;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.Optional;

@Data
@Valid
public class PaymentRequest {

    @JsonProperty(required = true)
    @Size(min = 1, max = 50)
    private String receiverId;

    @JsonProperty(required = true)
    private double amount;

    @Size(max = 5000)
    private String description;

}
