package com.example.qrcodeservice.restcontroller.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;

import lombok.Value;

import javax.validation.Valid;

@Value
@AllArgsConstructor
@Valid
public class PaymentResponse {

    private String receiverId;
    private double amount;

    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String description;


}
