package com.example.qrcodeservice.restcontroller.response;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class QRCodeResponse {
    private String qrcode;
}
